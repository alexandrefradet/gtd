import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActionTagsListComponent } from './action-tags-list/action-tags-list.component';

const routes: Routes = [{ path: '', component: ActionTagsListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TagsRoutingModule {
}
