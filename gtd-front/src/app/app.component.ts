import {Component, OnInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {Tag} from "./shared/store/tag.actions";
import {AuthenticatorService} from "@aws-amplify/ui-angular";

@Component({
  selector: 'gtd-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
      translate: TranslateService,
      private store: Store,
      private authenticator: AuthenticatorService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('en');
  }

  ngOnInit(): void {
    this.authenticator.subscribe(state => {
      if (state.authStatus === "authenticated") {
        this.loadRefData()
      }
    })
  }

  loadRefData() {
    this.store.dispatch(new Tag.FetchAll());
  }
}
