import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NextActionsRoutingModule } from './next-actions-routing.module';
import { NextActionsListComponent } from './next-actions-list/next-actions-list.component';
import { NextActionCardComponent } from './next-action-card/next-action-card.component';
import { NextActionDetailsComponent } from './next-action-details/next-action-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NextActionFormModule } from '../shared/next-action-form/next-action-form.module';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import {ClipboardModule} from "@angular/cdk/clipboard";
import {TranslateModule} from "@ngx-translate/core";
import {DeadlineColorPipe} from "../shared/pipes/deadline-color.pipe";

@NgModule({
  declarations: [
    NextActionsListComponent,
    NextActionCardComponent,
    NextActionDetailsComponent,
  ],
  imports: [
    CommonModule,
    NextActionsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NextActionFormModule,
    MatButtonModule,
    MatChipsModule,
    MatButtonToggleModule,
    MatCardModule,
    MatIconModule,
    ClipboardModule,
    TranslateModule,
    DeadlineColorPipe
  ]
})
export class NextActionsModule {
}
