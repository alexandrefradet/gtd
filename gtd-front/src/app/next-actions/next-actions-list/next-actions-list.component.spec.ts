import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextActionsListComponent } from './next-actions-list.component';

describe('NextActionsListComponent', () => {
  let component: NextActionsListComponent;
  let fixture: ComponentFixture<NextActionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextActionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextActionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
