import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActionType, NextAction} from '../../shared/next-action.model';
import {NextActionService} from '../../shared/next-action.service';
import {ActionTag} from '../../shared/action-tag.model';
import {ActionTagService} from '../../shared/action-tag.service';
import {Observable, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Select, Store} from '@ngxs/store';
import {NextActionFilter, NextActionStoreState} from '../../shared/store/next-action-store.state';
import {NextActionStore} from '../../shared/store/next-action-store.actions';
import {MatButtonToggleChange} from '@angular/material/button-toggle';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {TagStoreState} from "../../shared/store/tag-store.state";

@Component({
  selector: 'gtd-next-actions-list',
  templateUrl: './next-actions-list.component.html',
  styleUrls: ['./next-actions-list.component.scss']
})
export class NextActionsListComponent implements OnInit, OnDestroy {

  @Select(NextActionStoreState.getFilteredActions) filteredActions$: Observable<NextAction[]>;
  @Select(NextActionStoreState.getFilter) filter$: Observable<NextActionFilter>;
  @Select(TagStoreState.getTags) tags$ : Observable<ActionTag[]>;

  tags: ActionTag[];

  subs: Subscription[] = [];
  actionTypes = Object.keys(ActionType);
  filter: NextActionFilter;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private store: Store
  ) {
  }

  ngOnInit() {
    // TODO improve (resoleAll ?)
    this.subs.push(
      this.loadActions(),
      this.loadTags().subscribe(),
      this.filter$.pipe(
        tap(filter => this.filter = filter)
      ).subscribe()
    );
  }

  finishAction(id: number) {
    this.subs.push(
      this.store.dispatch(new NextActionStore.FinishAction(id)).pipe(
        tap(() => this.toastr.success("NextAction - Delete"))
      ).subscribe()
    );
  }

  finishActionAndProject(id: number) {
    this.subs.push(
      this.store.dispatch(new NextActionStore.FinishActionAndProject(id)).pipe(
        tap(() => this.toastr.success("NextActionAndProject - Delete"))
      ).subscribe()
    );
  }

  addNewAction(actionId: number, nextAction: NextAction) {
    this.subs.push(
      this.store.dispatch(new NextActionStore.FinishActionAndAddNew(actionId, nextAction)).pipe(
        tap(() => this.toastr.success('NextAction - AddNew'))
      ).subscribe()
    );
  }

  switchToStandard(action: NextAction) {
    const nextAction = new NextAction({
      ...action,
      actionType: ActionType.STANDARD
    });
    this.subs.push(
      this.store.dispatch(new NextActionStore.UpdateAction(nextAction)).pipe(
        tap(() => this.toastr.success('NextAction - Update'))
      ).subscribe()
    );
  }

  loadActions() {
    return this.store.dispatch(new NextActionStore.LoadAll(true)).subscribe();
  }

  loadTags(): Observable<ActionTag[]> {
    return this.tags$.pipe(
      tap(tags => this.tags = tags)
    )
  }

  toggleAction(newType: string) {
    this.applyfilter({
      actionType: ActionType[newType]
    })
  }

  toggleTag(event: MatButtonToggleChange) {
    const toggle = event.source;
    if (toggle) {
      const buttonGroup = toggle.buttonToggleGroup;
      if (event.value?.some(item => item == toggle.value)) {
        buttonGroup.value = [toggle.value];
        this.applyfilter({ tagId: toggle.value })
        return;
      }
    }
    this.applyfilter({ tagId: null });
  }

  applyfilter(newFilter: NextActionFilter) {
    this.subs.push(
      this.store.dispatch(new NextActionStore.Filter({
        ...this.filter,
        ...newFilter
      })).subscribe()
    );
  }

  editAction(id: number) {
    this.router.navigate(['/nextActions/', id]);
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
