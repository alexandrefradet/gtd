import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpBackend, HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {authInterceptorProviders} from './security/authentication.interceptor';
import {NgxsModule} from '@ngxs/store';
import {environment} from '../environments/environment';
import {IncomingStoreState} from './shared/store/incoming-store.state';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {NavigationModule} from './navigation/navigation.module';
import {NextActionStoreState} from './shared/store/next-action-store.state';
import {GlobalErrorHandler} from './shared/global-error-handler';
import {AmplifyAuthenticatorModule} from '@aws-amplify/ui-angular';
import {Amplify} from 'aws-amplify';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import localeFr from '@angular/common/locales/fr';
import {registerLocaleData} from "@angular/common";
import {HttpLoaderFactory} from "./core/translate-custom-http-loader";
import {TagStoreState} from "./shared/store/tag-store.state";
import {NgxsReduxDevtoolsPluginModule} from "@ngxs/devtools-plugin";

registerLocaleData(localeFr);
Amplify.configure(environment.amplify);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AmplifyAuthenticatorModule,
    ToastrModule.forRoot({
      timeOut: 500,
      preventDuplicates: true,
    }),
    NgxsModule.forRoot([
      IncomingStoreState,
      NextActionStoreState,
      TagStoreState
    ], {
      developmentMode: !environment.production
    }),
    MatButtonModule, // TODO for login component, create specific module and remove
    MatInputModule,
    NavigationModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpBackend]
      }
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: !environment.production
    })
  ],
  providers: [
    authInterceptorProviders,
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'fill' }
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
    { provide: LOCALE_ID, useValue: 'fr-FR'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
