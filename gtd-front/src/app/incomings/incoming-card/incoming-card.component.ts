import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Incoming } from '../incoming.model';

@Component({
  selector: 'gtd-incoming-card',
  templateUrl: './incoming-card.component.html',
  styleUrls: ['./incoming-card.component.scss']
})
export class IncomingCardComponent {

  @Input() incoming: Incoming;

  @Output('delete') deleteEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output('update') updateEvent: EventEmitter<void> = new EventEmitter<void>();

  deleteIncoming() {
    this.deleteEvent.emit()
  }

  update() {
    this.updateEvent.emit();
  }
}
