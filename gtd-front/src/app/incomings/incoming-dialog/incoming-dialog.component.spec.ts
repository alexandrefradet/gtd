import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingDialogComponent } from './incoming-dialog.component';

describe('IncomingDialogComponent', () => {
  let component: IncomingDialogComponent;
  let fixture: ComponentFixture<IncomingDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncomingDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
