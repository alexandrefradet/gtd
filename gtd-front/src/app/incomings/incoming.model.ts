export class Incoming {
  id: number;
  title: string;
  content: string;
  creationDate: Date;
  completionDate: Date;
  completed: boolean;

  public constructor(init?:Partial<Incoming>) {
    Object.assign(this, init);
  }
}
