import {Component, OnInit} from '@angular/core';
import {Incoming} from '../incoming.model';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {ProjectStatus} from '../../projects/project-status.enum';
import {Project} from '../../projects/project.model';
import {ToastrService} from 'ngx-toastr';
import {ActionType, NextAction} from '../../shared/next-action.model';
import {ActionTag} from '../../shared/action-tag.model';
import {combineLatest, Observable} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';
import {IncomingStoreState} from '../../shared/store/incoming-store.state';
import {Select, Store} from '@ngxs/store';
import {IncomingStore} from '../../shared/store/incoming-store.actions';
import {NextActionStore} from '../../shared/store/next-action-store.actions';
import {ProjectLight} from "../../shared/project-light.model";
import {TagStoreState} from "../../shared/store/tag-store.state";

@Component({
  selector: 'gtd-incoming-organizer',
  templateUrl: './incoming-organizer.component.html',
  styleUrls: ['./incoming-organizer.component.scss']
})
export class IncomingOrganizerComponent implements OnInit {

  @Select(IncomingStoreState.selectedIncoming) selectedIncoming$: Observable<Incoming>
  @Select(TagStoreState.getTags) tags$ : Observable<ActionTag[]>

  currentIncoming: Incoming = null;
  creationForm: UntypedFormGroup;
  availableTags: ActionTag[];

  isWaitingAction: boolean = false;

  constructor(
    private toastr: ToastrService,
    private fb: UntypedFormBuilder,
    private store: Store
  ) {
  }

  ngOnInit() {
    // TODO manage loadingAll before select (have to change page on comeback after F5)
    combineLatest([
      this.tags$.pipe(
        tap(tags => this.availableTags = tags),
        tap(()=> this.creationForm = this.generateNewForm())
      ),
      this.selectedIncoming$,
    ]).pipe(
      tap(r => {
        const incoming = r[1];
        this.currentIncoming = incoming;
        if (incoming != null) {
          this.creationForm.patchValue({
            project: this.defaultProject(),
            nextAction: new NextAction({
              ...this.defaultNextAction(),
              label: incoming?.title || '',
              content: incoming?.content || '',
            })
          })
        }
      })
    ).subscribe()

    this.store.dispatch(new IncomingStore.LoadAll()).pipe(
      switchMap(() => this.store.dispatch(new IncomingStore.SelectNext()))
    ).subscribe();
  }

  executeDelete(incoming: Incoming, deleteOnly: boolean = false) {
    return this.delete(incoming, deleteOnly)
      .subscribe()
  }

  delete(incoming: Incoming, deleteOnly: boolean = false) {
    return this.store.dispatch(new IncomingStore.Delete(incoming.id))
      .pipe(
        tap(() => this.toastr.success('Incoming - Delete')),
        tap(() => {
          if (deleteOnly) this.store.dispatch(new IncomingStore.SelectNext())
        })
      )
  }

  /**
   * Handle form submission
   */
  submitForm() {
    const nextAction: NextAction = this.creationForm.get('nextAction').value;
    const project: ProjectLight = this.creationForm.get('project').value;
    const incoming: Incoming = this.currentIncoming;

    if (project?.name != "") {
      nextAction.project = project;
    }
    if (this.isWaitingAction) {
      nextAction.actionType = ActionType.WAITING;
    }

    this.saveAction(incoming, nextAction).subscribe();
    this.store.dispatch(new IncomingStore.SelectNext());
  }

  saveAction(incoming: Incoming, action: NextAction): Observable<any> {
    return this.store.dispatch(new NextActionStore.CreateAction(action))
      .pipe(
        tap(() => this.toastr.success('NextAction - Save')),
        switchMap(()=> this.store.dispatch(new IncomingStore.Delete(incoming.id)))
      )
  }

  /**
   * Create a tinkering / maybe action based on the incoming
   */
  sendToMaybe() {
    const maybeAction: NextAction = new NextAction({
      label: this.currentIncoming.title,
      actionType: ActionType.MAYBE,
      completed: false
    });
    this.saveAction(this.currentIncoming, maybeAction).subscribe();
    this.store.dispatch(new IncomingStore.SelectNext());
  }

  /**
   * Generate global form
   */
  generateNewForm(nextAction = this.defaultNextAction()) {
    return this.fb.group({
      project: this.newProjectForm(),
      nextAction: [nextAction]
    })
  }

  /**
   * Generate form group for project model
   */
  newProjectForm() {
    return this.fb.group({
        name: [''],
        status: [ProjectStatus.TASK_TODO]
      }
    )
  }

  defaultNextAction(): NextAction {
    return new NextAction({
      label: '',
      content: '',
      actionPosition: 1,
      deadlineDate: null,
      completed: false,
      actionType: ActionType.STANDARD,
      actionTags: [],
      project: null
    })
  }

  defaultProject(): Project {
    return new Project({
      name: '',
      status: ProjectStatus.TASK_TODO
    });
  }
}
