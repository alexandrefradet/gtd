import {TranslateLoader} from "@ngx-translate/core";
import {HttpBackend, HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

export class TranslateCustomHttpLoader implements TranslateLoader {
  constructor(private http: HttpBackend, public prefix: string = "/assets/i18n/", public suffix: string = ".json") {}

  public getTranslation(lang: string): Observable<Object> {
    return new HttpClient(this.http).get(`${this.prefix}${lang}${this.suffix}`);
  }
}

export function HttpLoaderFactory(http: HttpBackend) {
  return new TranslateCustomHttpLoader(http);
}
