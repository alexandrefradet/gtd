import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActionTag } from './action-tag.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActionTagService {

  private readonly BASE_URL: string = environment.baseUrl + "/rest/actionTags";

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<ActionTag[]> {
    return this.httpClient.get<ActionTag[]>(this.BASE_URL);
  }

  save(actionTag: ActionTag): Observable<ActionTag> {
    return this.httpClient.post<ActionTag>(this.BASE_URL, actionTag);
  }

  update(id: number, tag: ActionTag): Observable<ActionTag> {
    return this.httpClient.put<ActionTag>(`${this.BASE_URL}/${id}`, tag);
  }
}
