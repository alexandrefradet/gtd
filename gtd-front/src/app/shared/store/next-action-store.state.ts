import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {NextActionService} from '../next-action.service';
import {ActionType, NextAction} from '../next-action.model';
import {mergeMap, tap} from 'rxjs/operators';
import {NextActionStore} from './next-action-store.actions';
import {ProjectService} from '../../projects/project.service';
import {of} from "rxjs";

export interface NextActionStoreStateModel {
  allNextActions: NextAction[],
  filteredActions: NextAction[],
  selectedAction: NextAction,
  filter: NextActionFilter
}

export interface NextActionFilter {
  tagId?: number,
  actionType?: ActionType
}

type Context = StateContext<NextActionStoreStateModel>;

@State<NextActionStoreStateModel>({
  name: 'nextAction',
  defaults: {
    allNextActions: [],
    filteredActions: [],
    selectedAction: null,
    filter: {
      actionType: ActionType.STANDARD
    }
  }
})
@Injectable()
export class NextActionStoreState {

  constructor(
    private nextActionService: NextActionService,
    private projectService: ProjectService
  ) {
  }

  @Selector()
  public static getFilteredActions(state: NextActionStoreStateModel): NextAction[] {
    return state.filteredActions;
  }

  @Selector()
  public static getFilter(state: NextActionStoreStateModel): NextActionFilter {
    return state.filter;
  }

  @Selector()
  public static getSelected(state: NextActionStoreStateModel): NextActionFilter {
    return state.selectedAction;
  }

  @Action(NextActionStore.LoadAll)
  public loadAllAction(ctx: Context, action: NextActionStore.LoadAll) {
    if (action.force || ctx.getState().allNextActions?.length === 0) {
      return this.nextActionService.getAll().pipe(
        tap(allNextActions => ctx.patchState({ allNextActions })),
        tap(() => this.applyFilter(ctx)))
    }
    return of();
  }

  @Action(NextActionStore.Filter)
  public filterIncoming(ctx: Context, { filter }: NextActionStore.Filter) {
    ctx.patchState({ filter });
    this.applyFilter(ctx);
  }

  @Action(NextActionStore.FinishAction)
  public finishAction(ctx: Context, action: NextActionStore.FinishAction) {
    return this.nextActionService.delete(action.actionId).pipe(
      tap(() => ctx.patchState({
        allNextActions: ctx.getState().allNextActions.filter(a => a.id != action.actionId)
      })),
      tap(() => this.applyFilter(ctx))
    );
  }

  @Action(NextActionStore.FinishActionAndProject)
  public finishActionAndProject(ctx: Context, action: NextActionStore.FinishActionAndProject) {
    const project = ctx.getState().filteredActions.find(a => a.id == action.actionId).project;
    return this.nextActionService.delete(action.actionId).pipe(
      tap(() => {
        ctx.patchState({
          allNextActions: ctx.getState().allNextActions.filter(a => a.id !== action.actionId)
        })
      }),
      tap(() => this.applyFilter(ctx)),
      mergeMap(() => this.projectService.delete(project.id)) // TODO project state ?
    );
  }

  @Action(NextActionStore.FinishActionAndAddNew)
  public finishActionAndAddNew(ctx: Context, action: NextActionStore.FinishActionAndAddNew) {
    const nextAction = action.newAction
    return this.nextActionService.delete(action.actionId).pipe(
      tap(() => ctx.patchState({
        allNextActions: ctx.getState().allNextActions.filter(a => a.id !== action.actionId)
      })),
      tap(() => this.applyFilter(ctx)),
      mergeMap(() => ctx.dispatch(new NextActionStore.CreateAction(nextAction))) // TODO check not emitting twice for snackbar ?
    );
  }

  @Action(NextActionStore.CreateAction)
  public createAction(ctx: Context, action: NextActionStore.CreateAction) {
    return this.nextActionService.save(action.newAction).pipe(
      tap(nextAction => ctx.patchState({
        allNextActions: [...ctx.getState().allNextActions, nextAction]
      })),
      tap(() => this.applyFilter(ctx))
    )
  }

  @Action(NextActionStore.UpdateAction)
  public updateAction(ctx: Context, action: NextActionStore.UpdateAction) {
    const actionId = action.action.id;
    return this.nextActionService.update(action.action).pipe(
      tap(nextAction => ctx.patchState({
        allNextActions: [...ctx.getState().allNextActions.filter(a => a.id !== actionId), nextAction]
      })),
      tap(() => this.applyFilter(ctx))
    )
  }

  @Action(NextActionStore.Select)
  public selectAction(ctx: Context, action: NextActionStore.Select) {
    if (!!action.actionId) {
      const currentAction = ctx.getState().allNextActions.find(a => a.id == action.actionId);
      ctx.patchState({
        selectedAction: new NextAction({
          ...currentAction,
          actionPosition: currentAction.actionPosition + 1 // TODO why ?
        })
      })
    } else {
      ctx.patchState({
        selectedAction: new NextAction({
          actionPosition: 1,
          completed: false,
          actionType: ActionType.STANDARD,
          actionTags: []
        })
      })
    }
  }

  applyFilter(ctx: Context) {
    const actions = ctx.getState().allNextActions ?? [];
    const filter = ctx.getState().filter ?? {};

    if (!actions || actions.length === 0) {
      ctx.patchState({ filteredActions: [] })
    }

    if (!filter) {
      ctx.patchState({ filteredActions: actions })
    }

    const filterAction: (action: NextAction) => boolean = !!filter.actionType ?
      action => action.actionType === filter.actionType :
      _ => true;

    const filterTag: (action: NextAction) => boolean = !!filter.tagId ?
      action => !!action.actionTags?.find(t => t.id == filter.tagId) : // TODO fix to use strict equality
      _ => true;

    ctx.patchState({
      filteredActions: actions.filter(action => filterTag(action) && filterAction(action))
    })
  }
}
