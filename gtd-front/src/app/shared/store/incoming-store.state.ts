import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { IncomingService } from '../../incomings/incoming.service';
import { tap } from 'rxjs/operators';
import { Incoming } from '../../incomings/incoming.model';
import { IncomingStore } from './incoming-store.actions';
import { BatchService } from '../batch/batch.service';

export interface IncomingStoreStateModel {
  incomings: Incoming[],
  selectedIncoming: Incoming
}

type Context = StateContext<IncomingStoreStateModel>;

const defaultIncoming = new Incoming({
  id: null,
  title: '',
  content: '',
  completionDate: null,
  creationDate: null,
  completed: false
});

@State<IncomingStoreStateModel>({
  name: 'incoming',
  defaults: {
    incomings: [],
    selectedIncoming: new Incoming(defaultIncoming)
  }
})
@Injectable()
export class IncomingStoreState {

  constructor(
    private incomingService: IncomingService,
    private batchService: BatchService
  ) {
  }

  @Selector()
  public static getIncomings(state: IncomingStoreStateModel): Incoming[] {
    return state.incomings;
  }

  @Selector()
  public static selectedIncoming(state: IncomingStoreStateModel): Incoming {
    return state.selectedIncoming;
  }

  @Action(IncomingStore.Select)
  public select(ctx: Context, action: IncomingStore.Select) {
    if (action?.incomingId) {
      return ctx.patchState({
        selectedIncoming: ctx.getState().incomings.find(i => i.id === action.incomingId)
      });
    } else {
      return ctx.patchState({
        selectedIncoming: new Incoming(defaultIncoming)
      })
    }
  }

  @Action(IncomingStore.SelectNext)
  public selectNext(ctx: Context, _: IncomingStore.SelectNext) {
    const incomingLength = ctx.getState().incomings.length;
    let selectedIncoming: Incoming = null;

    if (incomingLength > 0) {
      if (
        ctx.getState().selectedIncoming == null ||
        ctx.getState().incomings[0].id !== ctx.getState().selectedIncoming.id
      ) {
        selectedIncoming = ctx.getState().incomings[0]
      } else if (incomingLength > 1) {
        selectedIncoming = ctx.getState().incomings[1]
      }
    }
    ctx.patchState({
      selectedIncoming
    })
  }

  @Action(IncomingStore.LoadAll)
  public loadAllIncoming(ctx: Context) {
    return this.incomingService.getAll().pipe(
      tap(incomings => {
        ctx.patchState({ incomings })
      })
    )
  }

  @Action(IncomingStore.Delete)
  public deleteIncoming(ctx: Context, action: IncomingStore.Delete) {
    const incomingId = action.incomingId;
    return this.incomingService.delete(incomingId).pipe(
      tap(() => ctx.patchState({
        incomings: ctx.getState().incomings.filter(i => i.id !== incomingId)
      }))
    );
  }

  @Action(IncomingStore.Save)
  public save(ctx: Context, action: IncomingStore.Save) {
    return this.incomingService.save(action.incoming).pipe(
      tap(incoming => {
        ctx.patchState({
            incomings: [...ctx.getState().incomings.filter(i => i.id !== incoming.id), incoming],
          }
        )
      })
    )
  }

  @Action(IncomingStore.SaveAll)
  public saveAll(ctx: Context, action: IncomingStore.SaveAll) {
    return this.batchService.saveIncomings(action.incomings).pipe(
      tap(incomings => {
        const ids = incomings.map(i => i.id);
        ctx.patchState({
            incomings: [...ctx.getState().incomings.filter(i => !ids.includes(i.id)), ...incomings],
          }
        )
      })
    )
  }
}
