import {ActionTag} from "../action-tag.model";
import {ActionTagService} from "../action-tag.service";
import {Action, Selector, State, StateContext} from "@ngxs/store";
import {Tag} from "./tag.actions";
import {tap} from "rxjs/operators";
import {Injectable} from "@angular/core";

export interface TagStoreStateModel {
  tags: ActionTag[]
}

type Context = StateContext<TagStoreStateModel>;

@State<TagStoreStateModel>({
  name: 'tag',
  defaults: {
    tags: []
  }
})
@Injectable()
export class TagStoreState {

  constructor(private tagService: ActionTagService) {
  }

  @Selector()
  public static getTags(state: TagStoreStateModel) {
    return state.tags;
  }

  @Action(Tag.FetchAll)
  public loadTags(ctx: Context) {
    return this.tagService.getAll()
    .pipe(
        tap(tags => ctx.patchState({tags}))
    )
  }

  @Action(Tag.Save)
  public saveTag(ctx: Context, {tag}: Tag.Save) {
    return tag.id != null ?
        this.tagService.update(tag.id, tag)
        .pipe(
            tap(updatedTag => ctx.patchState({
              tags: [...ctx.getState().tags.filter(t => t.id !== updatedTag.id), updatedTag]
            }))
        ) :
        this.tagService.save(tag).pipe(
            tap(newTag => ctx.patchState({
                  tags: [...ctx.getState().tags, newTag]
            }))
        );
  }
}
