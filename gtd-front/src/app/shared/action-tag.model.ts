export class ActionTag {

  id: number;
  label: string;
  color: string;
  icon: string;
}
