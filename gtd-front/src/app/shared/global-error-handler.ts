import {ErrorHandler, Inject, Injectable, Injector} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ApiError} from './api-error.model';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandler implements ErrorHandler {

  private toastrService: ToastrService;

  constructor(@Inject(Injector) private injector: Injector,
  ) {
  }

  // Need to get ToastrService from injector rather than constructor injection to avoid cyclic dependency error
  private get toastr(): ToastrService {
    if (!this.toastrService) {
      this.toastrService = this.injector.get(ToastrService);
    }
    return this.toastrService;
  }

  handleError(error: any): void {
    console.error(error);
    if (error instanceof HttpErrorResponse) {
      const err = error as HttpErrorResponse;
      if (err.status == 401) {
        this.toastr.error(null, "Forgot you, need to login again", {
          timeOut: 3000
        });
        window.location.reload();
      } else if (error.error instanceof ApiError) {
        const {errorMessage}: ApiError = error.error;
        this.toastr.error(null, "Backend error : " + errorMessage, {
          timeOut: 3000
        });
      } else {
        const errorMessage = error?.message ?? 'Undefined client error';
        this.toastr.error(null, "Backend error : " + errorMessage, {
          timeOut: 3000
        });
      }
    } else {
      const message = error.rejection ?? error.message ?? error;
      this.toastr.error(null, "Internal error : " + message, {
        timeOut: 3000
      });
    }
  }
}
