import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NextActionFormComponent } from './next-action-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatChipsModule } from '@angular/material/chips';
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatButtonModule} from "@angular/material/button";
import {MatNativeDateModule} from "@angular/material/core";

@NgModule({
  declarations: [
    NextActionFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    TranslateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatNativeDateModule
  ],
  exports: [NextActionFormComponent]
})
export class NextActionFormModule {
}
