import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ActionType, NextAction} from '../next-action.model';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  UntypedFormBuilder,
  UntypedFormGroup,
  ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';
import {ActionTag} from '../action-tag.model';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'gtd-next-action-form',
  templateUrl: './next-action-form.component.html',
  styleUrls: ['./next-action-form.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => NextActionFormComponent)
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => NextActionFormComponent),
      multi: true
    }
  ]
})
export class NextActionFormComponent implements OnInit, ControlValueAccessor, Validator {

  @Input()
  tags: ActionTag[];

  nextAction: NextAction;
  nextActionForm: UntypedFormGroup;
  isReady = false;
  minDate = new Date();
  onTouched = () => {
  };
  onChange = (value: any) => {
  };

  constructor(
    private fb: UntypedFormBuilder
  ) {
  }

  ngOnInit(): void {
    this.nextActionForm = this.generateNewForm();
    this.nextActionForm.valueChanges.pipe(
      tap(value => {
        value.actionTags = value.actionTags
          .map((selected, index) => selected ? this.tags[index] : null)
          .filter(tag => tag != null);
        this.onChange(value)
      })
    ).subscribe();
    this.isReady = true;
  }

  /**
   * Generate global form
   */
  generateNewForm() {
    return this.fb.group({
      id: [''],
      label: ['', Validators.required],
      content: [''],
      completed: [false],
      completionDate: [''],
      creationDate: [''],
      deadlineDate: [''],
      actionPosition: [''],
      actionType: [ActionType.STANDARD],
      project: [''],
      actionTags: this.fb.array(this.tags.map(() => this.fb.control(false)))
    });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  validate(control: AbstractControl): ValidationErrors {
    return this.nextActionForm?.valid ?
      null :
      {
        invalidForm: {
          message: "Missing next action informations"
        }
      }
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(nextAction: NextAction): void {
    if (nextAction && this.isReady) {
      this.nextAction = nextAction;
      this.nextActionForm.patchValue({
        ...nextAction,
        actionTags: this.tags.map((tag) => nextAction?.actionTags?.some(t => t.id === tag.id))
      });
    }
  }
}
