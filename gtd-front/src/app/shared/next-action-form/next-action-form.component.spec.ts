import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextActionFormComponent } from './next-action-form.component';

describe('NextActionFormComponent', () => {
  let component: NextActionFormComponent;
  let fixture: ComponentFixture<NextActionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextActionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextActionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
