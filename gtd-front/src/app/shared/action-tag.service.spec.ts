import { TestBed } from '@angular/core/testing';

import { ActionTagService } from './action-tag.service';

describe('ActionTagService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActionTagService = TestBed.get(ActionTagService);
    expect(service).toBeTruthy();
  });
});
