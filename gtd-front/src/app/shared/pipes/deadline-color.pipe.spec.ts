import { DeadlineColorPipe } from './deadline-color.pipe';

describe('DeadlineColorPipe', () => {

  const pipe = new DeadlineColorPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return primary if not date', () => {
    expect(pipe.transform(null)).toBeUndefined();
  });

  it('should return primary if date is more than 2 weeks', () => {
    const date = new Date()
    date.setDate(date.getDate() + 15);
    expect(pipe.transform(date)).toEqual('primary');
  });

  it('should return warn if date is between one and two weeks', () => {
    const date = new Date()
    date.setDate(date.getDate() + 20);
    expect(pipe.transform(date)).toEqual('accent');
  });

  it('should return alert date if less than a week', () => {
    const date = new Date()
    date.setDate(date.getDate() + 6);
    expect(pipe.transform(date)).toEqual('warn');
  });
});
