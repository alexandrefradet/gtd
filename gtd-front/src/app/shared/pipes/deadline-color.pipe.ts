import { Pipe, PipeTransform } from '@angular/core';
import {ThemePalette} from "@angular/material/core";

@Pipe({
  name: 'deadlineColor',
  standalone: true
})
export class DeadlineColorPipe implements PipeTransform {
  readonly dayInMs = 86400000;

  transform(value: Date): ThemePalette {
    if (!value) {
      return 'primary';
    }

    const differentialDate = new Date(value).valueOf() - Date.now();
    if (differentialDate > (this.dayInMs*14)) {
      return 'primary';
    } else if (differentialDate > (this.dayInMs*7)) {
      return 'accent';
    }
    return 'warn';
  }

}
