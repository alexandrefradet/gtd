import { HttpStatusCode } from '@angular/common/http';

export interface BatchResult<T> {
  body: T;
  statusCode: HttpStatusCode;
}
