import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BatchRequest } from './batch-request';
import { environment } from '../../../environments/environment';
import { Incoming } from '../../incomings/incoming.model';
import { BatchResponse } from './batch-response';
import { BatchOperation } from './batch-operation';
import { HttpMethod } from './http-method';

@Injectable({
  providedIn: 'root'
})
export class BatchService {

  private readonly INCOMING_BATCH_URL: string = environment.baseUrl + "/rest/incomings/batch";

  constructor(private httpClient: HttpClient) {
  }

  saveIncomings(incomings: Incoming[]): Observable<Incoming[]> {
    const request = new BatchRequest({
      operations: incomings.map(body => new BatchOperation({
          body,
          method: body.id ? HttpMethod.PUT : HttpMethod.POST
        })
      )
    });

    return this.httpClient.post<BatchResponse<Incoming>>(this.INCOMING_BATCH_URL, request)
      .pipe(
        map(response => response.results.map(result => result.body))
      )
  }
}
