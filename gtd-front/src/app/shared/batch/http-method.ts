export enum HttpMethod {
  DELETE = 'DELETE',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  GET = 'GET',
  HEAD = 'HEAD'
}
