import { HttpMethod } from './http-method';

export class BatchOperation<T> {

  body: T;
  method: HttpMethod;

  public constructor(init?: Partial<BatchOperation<T>>) {
    Object.assign(this, init);
  }
}
