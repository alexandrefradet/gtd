export class ApiError {
  status: number;
  errorMessage: string;
  technicalMessage: string;
}
