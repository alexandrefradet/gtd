import {Pipe, PipeTransform} from '@angular/core';
import {ChecklistElement} from '../checklist-element.model';

@Pipe({
    name: 'ckeDisplay',
    // else don't refresh on element checked changed
    pure: false
})
export class ChecklistElementDisplayPipe implements PipeTransform {

    transform(element: ChecklistElement, showUncheck: boolean, search = ''): boolean {
        const searchTrimed = search?.trim()?.toLowerCase() ?? '';
        return (showUncheck || !element.checked) &&
            (searchTrimed.length < 1 || element.label.toLowerCase().includes(searchTrimed));
    }
}
