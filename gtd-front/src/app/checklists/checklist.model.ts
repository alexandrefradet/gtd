import { ChecklistElement } from './checklist-element.model';

export class Checklist {

  id: number;

  name: string;

  creationDate: Date;

  lastResetDate: Date;

  resetFrequencyUnit: string;

  resetFrequencyValue: number;

  checklistElements: ChecklistElement[] = [];
}
