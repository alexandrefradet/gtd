import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChecklistsListComponent } from './checklists-list/checklists-list.component';
import { ChecklistDetailsComponent } from './checklist-details/checklist-details.component';

const routes: Routes = [
  { path: '', component: ChecklistsListComponent },
  { path: ':id', component: ChecklistDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChecklistRoutingModule { }
