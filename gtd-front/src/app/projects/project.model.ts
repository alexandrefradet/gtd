import { ProjectStatus } from './project-status.enum';
import { NextAction } from '../shared/next-action.model';

export class Project {
  id: number;
  name: string;
  creationDate: Date;
  completionDate: Date;
  status: ProjectStatus;
  actions: NextAction[];

  public constructor(init?:Partial<Project>) {
    Object.assign(this, init);
  }
}
