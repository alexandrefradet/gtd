import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectCardComponent } from './project-card/project-card.component';
import { NextActionFormModule } from '../shared/next-action-form/next-action-form.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import {MatIconModule} from "@angular/material/icon";
import {ClipboardModule} from "@angular/cdk/clipboard";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    ProjectsListComponent,
    ProjectCardComponent,
  ],
    imports: [
        CommonModule,
        ProjectsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NextActionFormModule,
        MatButtonModule,
        MatCardModule,
        MatListModule,
        MatDialogModule,
        MatIconModule,
        ClipboardModule,
      TranslateModule
    ]
})
export class ProjectsModule {
}
