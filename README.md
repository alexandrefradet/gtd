# GTD - Getting things done

## What ? Why ? How ?
I'm tring to make a easy-to-use, responsive and power-user friendly (ie: custom to me, at first) to use the [Getting Things Done](https://gettingthingsdone.com/) methodology for my personal and professional organization (i'm currently reading the book).

Front will be Angular 8 (probably 9 later)
Backend will be Java 11 with Spring-Boot
At some point i'll dockerize everything.
The aim is also to learn to use gitlab-ci effectively.

Feel free to fork or try it for you !

## Native
```shell
# Binary
mvn -Pnative native:compile -DskipTests
DB_HOSTNAME=172.17.0.3 SPRING_PROFILES_ACTIVE=prod DB_USERNAME=postgres DB_PASSWORD=postgres DB_PORT=5432 spring_security_oauth2_resourceserver_jwt_issueruri=https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_JtrGvKq5k spring_security_oauth2_resourceserver_jwt_jwkseturi=https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_JtrGvKq5k/.well-known/jwks.json DB_NAME=application ./gtd-back
# Docker
mvn -Pnative spring-boot:build-image -DskipTests
docker run --rm -p 8080:8080 -e DB_HOSTNAME=172.17.0.3 -e SPRING_PROFILES_ACTIVE=prod \
-e DB_USERNAME=postgres -e DB_PASSWORD=postgres -e DB_PORT=5432 \
-e spring.security.oauth2.resourceserver.jwt.issuer-uri=https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_JtrGvKq5k \
-e spring.security.oauth2.resourceserver.jwt.jwk-set-uri=https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_JtrGvKq5k/.well-known/jwks.json \
-e DB_NAME=application docker.io/library/gtd-back:0.0.1-SNAPSHOT
```

## Utils

### App
- Run App (after mvn build) 
```shell script
java -jar -Dspring.profiles.active=production gtd-back/target/gtd-back-0.0.1-SNAPSHOT.jar
```

### Database
- Run postgres database with docker :
```shell script
docker run --name gtd-postgres-dev -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=gtd -p 5432:5432 -d postgres
```
- **rebuil_dev_db.sh** : stop, delete and recreate postgres db container named gtd-postgres-dev

### Libs
[ngx-toast](https://www.npmjs.com/package/ngx-toastr#options) : success, error, info, warning

### GraalVM
```shell
  sdk install java # use yes
```
- bashrc :
```
  export GRAALVM_HOME=$HOME/.sdkman/candidates/java/current
  export PATH=${GRAALVM_HOME}/bin:$PATH
```
```shell
install native : ${GRAALVM_HOME}/bin/gu install native-image
```

```shell
python3 -m pip install localstack
```
https://docs.localstack.cloud/get-started/cockpit/
