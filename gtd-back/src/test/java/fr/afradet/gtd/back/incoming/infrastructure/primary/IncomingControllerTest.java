package fr.afradet.gtd.back.incoming.infrastructure.primary;

import static fr.afradet.gtd.back.incoming.fixture.IncomingFixture.incomingEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.afradet.gtd.back.incoming.domain.Incoming;
import fr.afradet.gtd.back.incoming.infrastructure.secondary.IncomingEntity;
import fr.afradet.gtd.back.shared.ControllerTest;
import fr.afradet.gtd.back.shared.batch.domain.HttpMethod;
import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchOperationDTO;
import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchRequestDTO;
import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchResponseDTO;
import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchResultDTO;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
class IncomingControllerTest extends ControllerTest {

    private final LocalDateTime NOW = LocalDateTime.now();

    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

    public List<IncomingEntity> init() {
        return incomingRepository.saveAll(List.of(
            incomingEntity().setCompleted(false),
            incomingEntity().setCompleted(false),
            incomingEntity().setCompleted(true)
        ));
    }

    @Test
    void getAllChecklistsSecurityCheck() throws Exception {
        mvc.perform(get("/rest/incomings")
                        .contentType((MediaType.APPLICATION_JSON)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void getAllIncomings() throws Exception {
        var incomings = init();
        mvc.perform(get("/rest/incomings")
                        .with(userGtd())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].title", is(incomings.get(0).getTitle())))
                .andExpect(jsonPath("$[0].content", is(incomings.get(0).getContent())));
    }

    @Test
    void getIncoming() throws Exception {
        // GIVEN
        var incoming = incomingRepository.save(incomingEntity());

        // WHEN/THEN
        mvc.perform(get("/rest/incomings/%s".formatted(incoming.getId()))
                        .with(userGtd())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", is(incoming.getTitle())))
                .andExpect(jsonPath("$.content", is(incoming.getContent())));
    }

    @Test
    void createIncoming() throws Exception {
        String resultString = mvc.perform(post("/rest/incomings")
                        .with(userGtd())
                        .with(csrf())
                        .content(asJsonString(new Incoming().setTitle("Title3").setContent("Content3").setCompleted(false)))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("Title3")))
                .andExpect(jsonPath("$.content", is("Content3")))
                .andExpect(jsonPath("$.id", notNullValue()))
                .andReturn()
                .getResponse()
                .getContentAsString();

        Incoming resultDto = mapper.readValue(resultString, Incoming.class);
        assertThat(incomingRepository.findById(resultDto.getId())).hasValueSatisfying(i -> {
            assertThat(i.getTitle()).isEqualTo("Title3");
            assertThat(i.getContent()).isEqualTo("Content3");
            assertThat(i.getCompleted()).isFalse();
        });
    }

    @Test
    void updateIncoming() throws Exception {
        var incoming = incomingRepository.save(incomingEntity());
        mvc.perform(put("/rest/incomings/%s".formatted(incoming.getId()))
                        .with(userGtd())
                        .with(csrf())
                        .content(asJsonString(new Incoming().setId(incoming.getId()).setTitle("Title2Update").setContent("Content2Update").setCompleted(false)))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("Title2Update")))
                .andExpect(jsonPath("$.content", is("Content2Update")))
//                .andExpect(jsonPath("$.id", is(incoming.getId())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(incomingRepository.findById(incoming.getId())).hasValueSatisfying(i -> {
            assertThat(i.getTitle()).isEqualTo("Title2Update");
            assertThat(i.getContent()).isEqualTo("Content2Update");
            assertThat(i.getCompleted()).isFalse();
        });
    }

    @Test
    void deleteIncoming() throws Exception {
        var incoming = incomingRepository.save(incomingEntity());
        mvc.perform(delete("/rest/incomings/%s".formatted(incoming.getId()))
                .with(userGtd())
                .with(csrf())
        );

        assertThat(incomingRepository.findById(incoming.getId())).hasValueSatisfying(i -> {
            assertThat(i.getCompleted()).isTrue();
            assertThat(i.getCompletionDate()).isNotNull();
        });
    }


    @Disabled("Fix batch update")
    @Test
    void batchNominalTest() throws Exception {
        var incomings = init();
        String resultString = mvc.perform(post("/rest/incomings/batch")
                .with(userGtd())
                .with(csrf())
                .content(asJsonString(new BatchRequestDTO<IncomingDTO>()
                    .setOperations(List.of(
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.POST)
                            .setBody(new IncomingDTO().setTitle("Title3").setContent("Content3").setCompleted(false)),
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.PUT)
                            .setBody(new IncomingDTO().setId(incomings.get(0).getId()).setTitle("Title1b").setCompleted(false)),
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.PUT)
                            .setBody(new IncomingDTO().setId(incomings.get(1).getId()).setTitle("Title2b").setContent("Content2b").setCompleted(true)),
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.POST)
                            .setBody(new IncomingDTO().setTitle("Title4").setCompleted(false))
                    ))
                ))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        List<BatchResultDTO<IncomingDTO>> results = mapper.readValue(resultString, new TypeReference<BatchResponseDTO<IncomingDTO>>() {
        }).getResults();

        assertThat(results)
            .hasSize(4)
            .allSatisfy(r -> assertThat(r.getStatusCode()).isEqualTo(HttpStatus.OK.value()))
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isNotNull();
                assertThat(r.getBody().getTitle()).isEqualTo("Title3");
                assertThat(r.getBody().getContent()).isEqualTo("Content3");
                assertThat(r.getBody().getCompleted()).isFalse();
                assertThat(r.getBody().getCreationDate()).isNotNull();
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isEqualTo(incomings.get(0).getId());
                assertThat(r.getBody().getTitle()).isEqualTo("Title1b");
                assertThat(r.getBody().getContent()).isNull();
                assertThat(r.getBody().getCompleted()).isFalse();
                assertThat(r.getBody().getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isEqualTo(incomings.get(1).getId());
                assertThat(r.getBody().getTitle()).isEqualTo("Title2b");
                assertThat(r.getBody().getContent()).isEqualTo("Content2b");
                assertThat(r.getBody().getCompleted()).isTrue();
                assertThat(r.getBody().getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isNotNull();
                assertThat(r.getBody().getTitle()).isEqualTo("Title4");
                assertThat(r.getBody().getContent()).isNull();
                assertThat(r.getBody().getCompleted()).isFalse();
                assertThat(r.getBody().getCreationDate()).isNotNull();
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .extracting(r -> r.getBody().getTitle()).containsExactly("Title3", "Title1b", "Title2b", "Title4");

        assertThat(incomingRepository.findAll())
            .hasSize(5)
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(results.get(0).getBody().getId());
                assertThat(r.getTitle()).isEqualTo("Title3");
                assertThat(r.getContent()).isEqualTo("Content3");
                assertThat(r.getCompleted()).isFalse();
                assertThat(r.getCreationDate()).isNotNull();
                assertThat(r.getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(incomings.get(0).getId());
                assertThat(r.getTitle()).isEqualTo("Title1b");
                assertThat(r.getContent()).isNull();
                assertThat(r.getCompleted()).isFalse();
                assertThat(r.getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(incomings.get(1).getId());
                assertThat(r.getTitle()).isEqualTo("Title2b");
                assertThat(r.getContent()).isEqualTo("Content2b");
                assertThat(r.getCompleted()).isTrue();
                assertThat(r.getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(results.get(3).getBody().getId());
                assertThat(r.getTitle()).isEqualTo("Title4");
                assertThat(r.getContent()).isNull();
                assertThat(r.getCompleted()).isFalse();
                assertThat(r.getCreationDate()).isNotNull();
                assertThat(r.getCompletionDate()).isNull();
            });
    }

    @Test
    void batchCreateOnlyTest() throws Exception {
        String resultString = mvc.perform(post("/rest/incomings/batch")
                .with(userGtd())
                .with(csrf())
                .content(asJsonString(new BatchRequestDTO<IncomingDTO>()
                    .setOperations(List.of(
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.POST)
                            .setBody(new IncomingDTO().setTitle("Title1").setContent("Content1").setCompleted(false)),
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.POST)
                            .setBody(new IncomingDTO().setTitle("Title2").setContent("Content2").setCompleted(false)),
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.POST)
                            .setBody(new IncomingDTO().setTitle("Title3").setContent("Content3").setCompleted(true))
                    ))
                ))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        List<BatchResultDTO<Incoming>> results = mapper.readValue(resultString, new TypeReference<BatchResponseDTO<Incoming>>() {
        }).getResults();
        assertThat(results)
            .hasSize(3)
            .allSatisfy(r -> assertThat(r.getStatusCode()).isEqualTo(HttpStatus.OK.value()))
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isNotNull();
                assertThat(r.getBody().getTitle()).isEqualTo("Title1");
                assertThat(r.getBody().getContent()).isEqualTo("Content1");
                assertThat(r.getBody().getCompleted()).isFalse();
                assertThat(r.getBody().getCreationDate()).isNotNull();
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isNotNull();
                assertThat(r.getBody().getTitle()).isEqualTo("Title2");
                assertThat(r.getBody().getContent()).isEqualTo("Content2");
                assertThat(r.getBody().getCompleted()).isFalse();
                assertThat(r.getBody().getCreationDate()).isNotNull();
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isNotNull();
                assertThat(r.getBody().getTitle()).isEqualTo("Title3");
                assertThat(r.getBody().getContent()).isEqualTo("Content3");
                assertThat(r.getBody().getCompleted()).isTrue();
                assertThat(r.getBody().getCreationDate()).isNotNull();
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .extracting(r -> r.getBody().getTitle()).containsExactly("Title1", "Title2", "Title3");

        assertThat(incomingRepository.findAll())
            .hasSize(3)
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(results.get(0).getBody().getId());
                assertThat(r.getTitle()).isEqualTo("Title1");
                assertThat(r.getContent()).isEqualTo("Content1");
                assertThat(r.getCompleted()).isFalse();
                assertThat(r.getCreationDate()).isNotNull();
                assertThat(r.getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(results.get(1).getBody().getId());
                assertThat(r.getTitle()).isEqualTo("Title2");
                assertThat(r.getContent()).isEqualTo("Content2");
                assertThat(r.getCompleted()).isFalse();
                assertThat(r.getCreationDate()).isNotNull();
                assertThat(r.getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(results.get(2).getBody().getId());
                assertThat(r.getTitle()).isEqualTo("Title3");
                assertThat(r.getContent()).isEqualTo("Content3");
                assertThat(r.getCompleted()).isTrue();
                assertThat(r.getCreationDate()).isNotNull();
                assertThat(r.getCompletionDate()).isNull();
            });
    }

    @Disabled("Fix batch update")
    @Test
    void batchUpdateOnlyTest() throws Exception {
        var incomings = init();
        String resultString = mvc.perform(post("/rest/incomings/batch")
                .with(userGtd())
                .with(csrf())
                .content(asJsonString(new BatchRequestDTO<IncomingDTO>()
                    .setOperations(List.of(
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.PUT)
                            .setBody(new IncomingDTO().setId(incomings.get(2).getId()).setTitle("Title2b").setContent("Content2b").setCompleted(false)),
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.PUT)
                            .setBody(new IncomingDTO().setId(incomings.get(0).getId()).setTitle("Title3b").setCompleted(true)),
                        new BatchOperationDTO<IncomingDTO>()
                            .setMethod(HttpMethod.PUT)
                            .setBody(new IncomingDTO().setId(incomings.get(1).getId()).setTitle("Title1b").setContent("Content1b").setCompleted(false))
                    ))
                ))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        List<BatchResultDTO<IncomingDTO>> results = mapper.readValue(resultString, new TypeReference<BatchResponseDTO<IncomingDTO>>() {
        }).getResults();
        assertThat(results)
            .hasSize(3)
            .allSatisfy(r -> assertThat(r.getStatusCode()).isEqualTo(HttpStatus.OK.value()))
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isEqualTo(incomings.get(2).getId());
                assertThat(r.getBody().getTitle()).isEqualTo("Title2b");
                assertThat(r.getBody().getContent()).isEqualTo("Content2b");
                assertThat(r.getBody().getCompleted()).isFalse();
                assertThat(r.getBody().getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isEqualTo(incomings.get(0).getId());
                assertThat(r.getBody().getTitle()).isEqualTo("Title3b");
                assertThat(r.getBody().getContent()).isNull();
                assertThat(r.getBody().getCompleted()).isTrue();
                assertThat(r.getBody().getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getBody().getId()).isEqualTo(incomings.get(1).getId());
                assertThat(r.getBody().getTitle()).isEqualTo("Title1b");
                assertThat(r.getBody().getContent()).isEqualTo("Content1b");
                assertThat(r.getBody().getCompleted()).isFalse();
                assertThat(r.getBody().getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getBody().getCompletionDate()).isNull();
            })
            .extracting(r -> r.getBody().getId()).containsExactly(
                incomings.get(2).getId(),
                incomings.get(0).getId(),
                incomings.get(1).getId()
            );

        assertThat(incomingRepository.findAll())
            .hasSize(3)
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(incomings.get(2).getId());
                assertThat(r.getTitle()).isEqualTo("Title2b");
                assertThat(r.getContent()).isEqualTo("Content2b");
                assertThat(r.getCompleted()).isFalse();
                assertThat(r.getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(incomings.get(0).getId());
                assertThat(r.getTitle()).isEqualTo("Title3b");
                assertThat(r.getContent()).isNull();
                assertThat(r.getCompleted()).isTrue();
                assertThat(r.getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getCompletionDate()).isNull();
            })
            .anySatisfy(r -> {
                assertThat(r.getId()).isEqualTo(incomings.get(1).getId());
                assertThat(r.getTitle()).isEqualTo("Title1b");
                assertThat(r.getContent()).isEqualTo("Content1b");
                assertThat(r.getCompleted()).isFalse();
                assertThat(r.getCreationDate()).isEqualToIgnoringNanos(NOW);
                assertThat(r.getCompletionDate()).isNull();
            });
    }
}
