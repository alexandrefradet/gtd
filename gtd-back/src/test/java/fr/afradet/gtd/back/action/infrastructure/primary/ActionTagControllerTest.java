package fr.afradet.gtd.back.action.infrastructure.primary;

import static fr.afradet.gtd.back.action.fixture.ActionFixture.actionTagEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import fr.afradet.gtd.back.action.domain.ActionTag;
import fr.afradet.gtd.back.shared.ControllerTest;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
class ActionTagControllerTest extends ControllerTest {

  @Container
  @ServiceConnection
  static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

  @Test
  void getAllChecklistsSecurityCheck() throws Exception {
    mvc.perform(get("/rest/actionTags")
            .contentType((MediaType.APPLICATION_JSON)))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(roles = {"GTD_USER"})
  void getAllTags() throws Exception {
    var tags = actionTagRepository.saveAll(List.of(actionTagEntity(), actionTagEntity()));

    mvc.perform(get("/rest/actionTags")
//                        .with(userGtd())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].label", is(tags.get(0).getLabel())))
        .andExpect(jsonPath("$[0].color", is(tags.get(0).getColor())))
        .andExpect(jsonPath("$[0].icon", is(tags.get(0).getIcon())));
  }

  @Test
  void createTag() throws Exception {
    String actionTagString = mvc.perform(post("/rest/actionTags")
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(new ActionTagDTO().setLabel("NewLabel").setColor("#123456").setIcon("home")))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.label", is("NewLabel")))
        .andExpect(jsonPath("$.color", is("#123456")))
        .andExpect(jsonPath("$.icon", is("home")))
        .andExpect(jsonPath("$.id", notNullValue()))
        .andReturn()
        .getResponse()
        .getContentAsString();

    ActionTag actionTag = mapper.readValue(actionTagString, ActionTag.class);

    assertThat(actionTag.getId()).isNotNull();
    assertThat(actionTagRepository.findAll()).singleElement().satisfies(tag -> assertThat(tag.getId()).isEqualTo(actionTag.getId()));
  }

  @Test
  void updateTag() throws Exception {
    var tag = actionTagRepository.save(actionTagEntity());

    mvc.perform(put("/rest/actionTags/%s".formatted(tag.getId()))
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(new ActionTagDTO().setId(tag.getId()).setLabel("Label2").setColor("#789123").setIcon("tagada")))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.label", is("Label2")))
        .andExpect(jsonPath("$.color", is("#789123")))
        .andExpect(jsonPath("$.icon", is("tagada")));
//        .andExpect(jsonPath("$.id", is(-1)));

    assertThat(actionTagRepository.findAll()).singleElement().satisfies(t -> {
      assertThat(t.getLabel()).isEqualTo("Label2");
      assertThat(t.getColor()).isEqualTo("#789123");
      assertThat(t.getIcon()).isEqualTo("tagada");
    });
  }
}
