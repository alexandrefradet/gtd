package fr.afradet.gtd.back.action.fixture;

import fr.afradet.gtd.back.action.domain.ActionType;
import fr.afradet.gtd.back.action.infrastructure.secondary.ActionTagEntity;
import fr.afradet.gtd.back.action.infrastructure.secondary.NextActionEntity;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class ActionFixture {

  private static final List<String> icons = List.of("delete", "home", "phone");

  public static ActionTagEntity actionTagEntity() {
    var id = new Random().nextLong();
    return new ActionTagEntity().setLabel("Label %s".formatted(id)).setColor("#%S".formatted(id).substring(0, 7)).setIcon(icon());
  }

  public static NextActionEntity nextActionEntity() {
    var id = new Random().nextLong();
    return new NextActionEntity()
        .setActionType(ActionType.STANDARD)
        .setCompleted(false)
        .setLabel("lab %s".formatted(id))
        .setCreationDate(LocalDateTime.now());
  }

  private static String icon() {
    return icons.get(new Random().nextInt(icons.size()));
  }
}
