package fr.afradet.gtd.back.action.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import fr.afradet.gtd.back.action.aplication.ProjectService;
import fr.afradet.gtd.back.action.domain.EProjectStatus;
import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.NextActionRepository;
import fr.afradet.gtd.back.action.domain.Project;
import fr.afradet.gtd.back.action.domain.ProjectRepository;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ProjectServiceTest {

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private NextActionRepository nextActionRepository;

    @Captor
    ArgumentCaptor<Project> argCaptor;

    private ProjectService sut;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        sut = new ProjectService(projectRepository, nextActionRepository);
    }

    @Test
    void create_no_action_ok() {
        // GIVEN
        Project project = new Project().setName("My project");

        // WHEN
        sut.create(project, Collections.emptyList());

        // THEN
        verify(projectRepository).save(argCaptor.capture());
        Assertions.assertEquals(EProjectStatus.TASK_NEEDED, argCaptor.getValue().getStatus());
    }

    @Test
    void create_with_action_ok() {
        // Given
        when(projectRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);
        when(nextActionRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);
        Project project = new Project().setName("My project");

        // When
        sut.create(project, List.of(new NextAction().setLabel("My action")));

        // Then
        verify(projectRepository).save(argCaptor.capture());
        assertEquals(EProjectStatus.TASK_TODO, argCaptor.getValue().getStatus());
    }

    // TODO test project deletion if action still uncompleted, actions all completed, etc
}
