package fr.afradet.gtd.back.config;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Table;
import java.util.List;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.util.StringUtils;

public class ClearDatabaseExtension implements BeforeEachCallback {

  private EntityManager entityManager;
  private List<String> tableNames;

  public ClearDatabaseExtension() {
//    SpringExtension.getApplicationContext().getBean(SomeBean.class)
  }

  @Override
  public void beforeEach(ExtensionContext extensionContext) throws Exception {
    entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY = FALSE").executeUpdate();

    for (String tableName : tableNames) {
      entityManager.createNativeQuery("TRUNCATE TABLE " + tableName).executeUpdate();
    }

    entityManager.createNativeQuery("SET REFERENTIAL_INTEGRITY = TRUE").executeUpdate();
  }

  @PostConstruct
  void afterPropertiesSet() {
    tableNames = entityManager.getMetamodel().getEntities().stream()
        .filter(entityType -> entityType.getJavaType().getAnnotation(Table.class) != null)
            .map(entityType -> entityType.getJavaType().getAnnotation(Table.class))
            .map(this::convertToTableName) // TODO
        .toList();
  }

  private String convertToTableName(Table table) {
    String schema = table.schema();
    String tableName = table.name();

    String convertedSchema = StringUtils.hasText(schema) ? schema.toLowerCase() + "." : "";
    String convertedTableName = tableName.replaceAll("([a-z])([A-Z])", "$1_$2");

    return convertedSchema + convertedTableName;
  }
}
