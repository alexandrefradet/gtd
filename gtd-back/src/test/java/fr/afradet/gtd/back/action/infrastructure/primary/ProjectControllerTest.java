package fr.afradet.gtd.back.action.infrastructure.primary;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import fr.afradet.gtd.back.action.domain.ActionTag;
import fr.afradet.gtd.back.action.domain.ActionType;
import fr.afradet.gtd.back.action.domain.EProjectStatus;
import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.Project;
import fr.afradet.gtd.back.action.infrastructure.secondary.ActionTagEntity;
import fr.afradet.gtd.back.action.infrastructure.secondary.NextActionEntity;
import fr.afradet.gtd.back.action.infrastructure.secondary.ProjectEntity;
import fr.afradet.gtd.back.shared.ControllerTest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.http.MediaType;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
class ProjectControllerTest extends ControllerTest {

  @Container
  @ServiceConnection
  static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

  private List<ProjectEntity> projects;
  private List<ActionTagEntity> tags;

  @BeforeEach
  public void setUp() {
    super.setUp();
    projects = projectRepository.saveAll(List.of(
        new ProjectEntity().setId(-1L).setName("MyProject1").setStatus(EProjectStatus.TASK_TODO)
            .setCompleted(false),
        new ProjectEntity().setId(-2L).setName("MyProject2").setCompleted(false),
        new ProjectEntity().setId(-7L).setName("MyProject2").setCompleted(true)
    ));
    tags = actionTagRepository.saveAll(List.of(
        new ActionTagEntity().setId(-5L).setLabel("TAG1").setIcon("delete").setColor("#12345"),
        new ActionTagEntity().setId(-6L).setLabel("TAG2").setIcon("delete").setColor("#12345")
    ));

    nextActionRepository.saveAll(List.of(
        new NextActionEntity()
            .setId(-4L)
            .setProject(projects.get(0))
            .setLabel("NextActionLabel1")
            .setContent("NextActionContent1")
            .setActionType(ActionType.WAITING)
            .setActionPosition(1)
            .setCompleted(true)
            .setCreationDate(LocalDateTime.now()),
        new NextActionEntity()
            .setId(-3L)
            .setProject(projects.get(0))
            .setLabel("NextActionLabel2")
            .setContent("NextActionContent2")
            .setActionType(ActionType.STANDARD)
            .setActionPosition(2)
            .setCompleted(false)
            .setCreationDate(LocalDateTime.now().plusHours(3))
            .setActionTags(Set.of(tags.get(0), tags.get(1)))
    ));
  }

  @Test
  void getAllChecklistsSecurityCheck() throws Exception {
    mvc.perform(get("/rest/projects")
            .contentType((MediaType.APPLICATION_JSON)))
        .andExpect(status().isUnauthorized());
  }

  @Test
  void getAllProjects() throws Exception {
    mvc.perform(get("/rest/projects")
            .with(userGtd())
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content()
            .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].name", is("MyProject1")))
        .andExpect(jsonPath("$[0].actions", hasSize(2)))
        .andExpect(jsonPath("$[0].actions[0].label", is("NextActionLabel1")))
        .andExpect(jsonPath("$[0].actions[0].content", is("NextActionContent1")))
        .andExpect(jsonPath("$[0].actions[0].actionType", is("WAITING")));
  }

  @Disabled("Unused, endpoint removed")
  @Test
  void updateProject() throws Exception {
    mvc.perform(put("/rest/projects/-1")
            .with(userGtd())
            .with(csrf())
            .content(asJsonString(new ProjectDTO().setId(-1L).setName("ProjectUpdate")
                .setStatus(EProjectStatus.TASK_NEEDED.name())))
            .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name", is("ProjectUpdate")))
        .andExpect(jsonPath("$.status", is(EProjectStatus.TASK_NEEDED.name())))
        .andExpect(jsonPath("$.id", is(-1)));
  }

  @Test
  void deleteProject() throws Exception {
    var idProject = projects.get(0).getId();
    mvc.perform(delete("/rest/projects/%s".formatted(idProject))
        .with(userGtd())
        .with(csrf())

    );

    assertThat(projectRepository.findById(idProject)).hasValueSatisfying(project -> {
      assertThat(project.getId()).isEqualTo(idProject);
      assertThat(project.isCompleted()).isTrue();
      assertThat(project.getStatus()).isEqualTo(EProjectStatus.COMPLETED);
      assertThat(project.getCompletionDate()).isNotNull();
    });
  }
}
