package fr.afradet.gtd.back.checklist.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

class ChecklistTest {

  @Test
  void executeActionRest() {
    // GIVEN
    var checklist = new Checklist()
        .setChecklistElements(List.of(
            new ChecklistElement().setId(1L).setChecked(false),
            new ChecklistElement().setId(2L).setChecked(true)
        ));

    // WHEN
    checklist.executeAction(ChecklistActionType.RESET);

    // THEN
    assertThat(checklist.getChecklistElements())
        .allSatisfy(cke -> assertThat(cke.isChecked()).isFalse());
  }
}
