package fr.afradet.gtd.back.action.infrastructure.secondary;

import static fr.afradet.gtd.back.action.fixture.ActionFixture.actionTagEntity;
import static fr.afradet.gtd.back.action.fixture.ActionFixture.nextActionEntity;
import static org.assertj.core.api.Assertions.assertThat;

import fr.afradet.gtd.back.action.domain.ActionTag;
import fr.afradet.gtd.back.action.domain.ActionType;
import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.NextActionRepository;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@ActiveProfiles("test")
@Testcontainers
class NextActionRepositoryImplTest {

  @Container
  @ServiceConnection
  static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:15");

    @Autowired
    NextActionRepository sut;
    @Autowired
    ActionTagJpaRepository actionTagRepository;
    @Autowired
    NextActionJpaRepository nextActionRepository;

    private List<ActionTagEntity> tags;
    private List<NextActionEntity> actions;

    @BeforeEach
    void setup() {
      nextActionRepository.deleteAll();
      actionTagRepository.deleteAll();
      setupDatas();
    }

    @Test
    void findByCompleted() {
        assertThat(sut.findByCompleted(true))
            .hasSize(2)
            .extracting("id")
            .containsExactly(
                actions.get(3).getId(),
                actions.get(6).getId()
            );

      assertThat(sut.findByCompleted(false))
          .hasSize(5)
          .extracting("id")
          .containsExactly(
              actions.get(0).getId(),
              actions.get(1).getId(),
              actions.get(2).getId(),
              actions.get(4).getId(),
              actions.get(5).getId()
          );
    }

    @Test
    void findByCompletedAndTypeAndTagsId() {
        assertThat(
            sut.findByCompletedAndTypeAndTagsId(false, ActionType.WAITING, List.of(tags.get(1).getId()))
        )
            .singleElement()
            .satisfies(a -> assertThat(a.getId()).isEqualTo(actions.get(2).getId()));
    }

    private void setupDatas() {
        tags = actionTagRepository.saveAll(List.of(actionTagEntity(), actionTagEntity(), actionTagEntity()));

      actions = nextActionRepository.saveAll(List.of(
                nextActionEntity().setActionType(ActionType.STANDARD).setCompleted(false).setLabel("lab1").addTag(tags.get(0)),
                nextActionEntity().setActionType(ActionType.STANDARD).setCompleted(false).setLabel("lab2").addTag(tags.get(0)).addTag(tags.get(1)),
                nextActionEntity().setActionType(ActionType.WAITING).setCompleted(false).setLabel("lab3").addTag(tags.get(1)),
                nextActionEntity().setActionType(ActionType.WAITING).setCompleted(true).setLabel("lab3").addTag(tags.get(1)),
                nextActionEntity().setActionType(ActionType.STANDARD).setCompleted(false).setLabel("lab4").addTag(tags.get(1)),
                nextActionEntity().setActionType(ActionType.MAYBE).setCompleted(false).setLabel("lab5").addTag(tags.get(2)),
                nextActionEntity().setActionType(ActionType.STANDARD).setCompleted(true).setLabel("lab6").addTag(tags.get(1))
        ));
    }
}
