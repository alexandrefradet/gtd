package fr.afradet.gtd.back.incoming.fixture;

import fr.afradet.gtd.back.incoming.infrastructure.secondary.IncomingEntity;
import java.time.LocalDateTime;
import java.util.Random;

public class IncomingFixture {

  public static IncomingEntity incomingEntity() {
    var id = new Random().nextLong();
    return new IncomingEntity()
        .setTitle("Title %s".formatted(id))
        .setContent("Content %s".formatted(id))
        .setCompleted(false)
        .setCreationDate(LocalDateTime.now());
  }
}
