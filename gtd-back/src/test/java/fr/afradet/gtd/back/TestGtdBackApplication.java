package fr.afradet.gtd.back;

import static fr.afradet.gtd.back.action.fixture.ActionFixture.actionTagEntity;
import static fr.afradet.gtd.back.action.fixture.ActionFixture.nextActionEntity;
import static fr.afradet.gtd.back.incoming.fixture.IncomingFixture.incomingEntity;

import fr.afradet.gtd.back.action.infrastructure.secondary.ActionTagJpaRepository;
import fr.afradet.gtd.back.action.infrastructure.secondary.NextActionEntity;
import fr.afradet.gtd.back.action.infrastructure.secondary.NextActionJpaRepository;
import fr.afradet.gtd.back.incoming.infrastructure.secondary.IncomingEntity;
import fr.afradet.gtd.back.incoming.infrastructure.secondary.IncomingJpaRepository;
import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.devtools.restart.RestartScope;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.testcontainers.containers.PostgreSQLContainer;

public class TestGtdBackApplication {

  public static void main(String[] args) {
    SpringApplication.from(GtdBackApplication::main)
        .with(ContainerConfiguration.class)
        .run(args);
  }

  @TestConfiguration(proxyBeanMethods = false)
  public static class ContainerConfiguration {

    @Bean
    @ServiceConnection
    @RestartScope
    PostgreSQLContainer<?> postgreSQLContainer() {
      return new PostgreSQLContainer<>("postgres:15");
    }

    @Component
    public static class DataLoader implements ApplicationRunner {

      private final IncomingJpaRepository incomingRepository;
      private final NextActionJpaRepository nextActionRepository;
      private final ActionTagJpaRepository actionTagJpaRepository;

      public DataLoader(IncomingJpaRepository incomingRepository,
          NextActionJpaRepository nextActionRepository,
          ActionTagJpaRepository actionTagJpaRepository) {
        this.incomingRepository = incomingRepository;
        this.nextActionRepository = nextActionRepository;
        this.actionTagJpaRepository = actionTagJpaRepository;
      }


      public void run(ApplicationArguments args) {
        this.incomingRepository.saveAll(List.of(
            incomingEntity(),
            incomingEntity(),
            incomingEntity(),
            incomingEntity()
        ));
        var actionTags = new HashSet<>(this.actionTagJpaRepository.findAll());
        this.nextActionRepository.saveAll(List.of(
            nextActionEntity().setActionTags(new HashSet<>(actionTags)),
            nextActionEntity().setActionTags(new HashSet<>(actionTags)),
            nextActionEntity().setActionTags(new HashSet<>(actionTags)),
            nextActionEntity().setActionTags(new HashSet<>(actionTags))
        ));
      }
    }

  }
}
