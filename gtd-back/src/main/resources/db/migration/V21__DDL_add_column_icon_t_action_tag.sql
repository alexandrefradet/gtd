ALTER TABLE t_action_tag ADD COLUMN icon text;

UPDATE t_action_tag
SET icon =
        CASE label
            WHEN 'PC' THEN 'computer'
            WHEN 'FRIEND' THEN 'groups'
            WHEN 'HOME' THEN 'home'
            WHEN 'ERRAND' THEN 'shopping_cart'
            WHEN 'FAMILY' THEN 'family_restroom'
            WHEN 'PHONE' THEN 'phone'
            WHEN 'WORK' THEN 'work'
            ELSE 'home'
        END;

ALTER TABLE t_action_tag
    ALTER COLUMN icon SET NOT NULL;
