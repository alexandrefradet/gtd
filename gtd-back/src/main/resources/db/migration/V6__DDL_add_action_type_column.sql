-- Add action_type column to to next_action table

ALTER TABLE next_action ADD COLUMN action_type character varying(20) NOT NULL;
