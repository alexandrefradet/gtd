-- Create checklist sequence and table

CREATE SEQUENCE checklist_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE checklist
(
    id bigint NOT NULL DEFAULT nextval('checklist_id_seq'::regclass),
    creation_date date,
    last_reset_date date,
    name character varying(255) NOT NULL,
    reset_frequency_unit character varying(20),
    reset_frequency_value integer,
    CONSTRAINT checklist_pk_id PRIMARY KEY (id),
    CONSTRAINT checklist_uk_name UNIQUE (name)
)
