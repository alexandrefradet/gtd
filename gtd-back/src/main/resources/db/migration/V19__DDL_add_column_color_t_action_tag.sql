-- Add add color column to t_action_tag

ALTER TABLE t_action_tag ADD COLUMN color varchar(7) NOT NULL DEFAULT '#add8e6';
