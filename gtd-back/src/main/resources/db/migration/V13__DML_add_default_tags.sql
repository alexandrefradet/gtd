-- Add default tags

INSERT INTO action_tag(label)
VALUES ('HOME'),
       ('PC'),
       ('FAMILY'),
       ('FRIEND'),
       ('ERRAND');
