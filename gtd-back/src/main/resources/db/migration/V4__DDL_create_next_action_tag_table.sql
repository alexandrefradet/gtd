---Create "ActionTag" sequence, table, constraint

CREATE SEQUENCE action_tag_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE action_tag
(
    id bigint NOT NULL DEFAULT nextval('action_tag_id_seq'::regclass),
    label character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT action_tag_pk_d PRIMARY KEY (id),
    CONSTRAINT action_tag_uk_label UNIQUE (label)
)
