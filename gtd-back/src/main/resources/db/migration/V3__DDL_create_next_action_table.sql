-- Create "NextAction" sequence, table and foreign key on project

CREATE SEQUENCE next_action_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE next_action
(
    id bigint NOT NULL DEFAULT nextval('next_action_id_seq'::regclass),
    label character varying(255) NOT NULL,
    content character varying(255),
    action_position integer NOT NULL,
    creation_date timestamp without time zone,
    completion_date timestamp without time zone,
    completed boolean NOT NULL,
    project_id bigint,
    CONSTRAINT next_action_pk_id PRIMARY KEY (id),
    CONSTRAINT next_action_fk_project_id FOREIGN KEY (project_id)
        REFERENCES project (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
