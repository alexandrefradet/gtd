-- Add standard role to role_gtd

INSERT INTO role_gtd(name)
    VALUES
           ('ROLE_ADMIN'),
           ('ROLE_USER');
