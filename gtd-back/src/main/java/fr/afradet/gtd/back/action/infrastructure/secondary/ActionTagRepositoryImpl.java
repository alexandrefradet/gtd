package fr.afradet.gtd.back.action.infrastructure.secondary;

import fr.afradet.gtd.back.action.domain.ActionTag;
import fr.afradet.gtd.back.action.domain.ActionTagRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ActionTagRepositoryImpl implements ActionTagRepository {

  private final ActionTagJpaRepository repository;

  public ActionTagRepositoryImpl(ActionTagJpaRepository repository) {
    this.repository = repository;
  }

  @Override
  public List<ActionTag> findAll() {
    return repository.findAll().stream().map(ActionTagEntity::toDomain).toList();
  }

  @Override
  public ActionTag save(ActionTag actionTag) {
    return repository.save(ActionTagEntity.fromDomain(actionTag)).toDomain();
  }

  @Override
  public Optional<ActionTag> findById(Long id) {
    return repository.findById(id).map(ActionTagEntity::toDomain);
  }
}
