package fr.afradet.gtd.back.action.infrastructure.primary;

import fr.afradet.gtd.back.action.aplication.NextActionService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rest/nextActions")
@PreAuthorize("hasRole('GTD_USER') or hasRole('GTD_ADMIN')")
public class NextActionController {

    private final NextActionService nextActionService;
    private final NextActionMapper mapper;

    public NextActionController(NextActionService nextActionService, NextActionMapper mapper) {
        this.nextActionService = nextActionService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<NextActionDTO> getNextActions() {
        return mapper.toDtos(nextActionService.findAll());
    }

    @PostMapping
    public NextActionDTO createNextAction(@RequestBody @Valid NextActionDTO actionDto) {
        return mapper.toDto(nextActionService.create(mapper.toPojo(actionDto)));
    }

    @GetMapping("/{id}")
    public NextActionDTO getNextAction(@PathVariable("id") Long id) {
        return mapper.toDto(nextActionService.getById(id));
    }

    @PutMapping("/{id}")
    public NextActionDTO updateNextAction(@PathVariable("id") Long id, @RequestBody @Valid NextActionDTO dto) {
        return mapper.toDto(nextActionService.update(mapper.toPojo(dto), id));
    }

    @DeleteMapping("/{id}")
    public void deleteNextAction(@PathVariable("id") Long id) {
        nextActionService.delete(id);
    }
}
