package fr.afradet.gtd.back.incoming.infrastructure.primary;

import fr.afradet.gtd.back.incoming.application.IncomingService;
import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchRequestDTO;
import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchResponseDTO;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("rest/incomings")
@PreAuthorize("hasRole('GTD_USER') or hasRole('GTD_ADMIN')")
public class IncomingController {

    private final IncomingService incomingService;
    private final IncomingMapper mapper;

    public IncomingController(IncomingService incomingService, IncomingMapper mapper) {
        this.incomingService = incomingService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<IncomingDTO> getAllIncomings() {
        return mapper.toDtos(incomingService.getAllIncomings());
    }

    @GetMapping("/{id}")
    public IncomingDTO getIncoming(@PathVariable("id") Long id) {
        return mapper.toDto(incomingService.getIncoming(id));
    }

    @PostMapping
    public IncomingDTO createIncoming(@RequestBody @Valid IncomingDTO incoming) {
        return mapper.toDto(incomingService.create(mapper.toPojo(incoming)));
    }

    @PutMapping("/{id}")
    public IncomingDTO updateIncoming(@PathVariable("id") Long id, @RequestBody @Valid IncomingDTO incoming) {
        return mapper.toDto(incomingService.update(id, mapper.toPojo(incoming)));
    }

    @DeleteMapping("/{id}")
    public void deleteIncoming(@PathVariable("id") Long id) {
        incomingService.delete(id);
    }

    @PostMapping("/batch")
    public BatchResponseDTO<IncomingDTO> batchIncomings(@RequestBody BatchRequestDTO<IncomingDTO> incomingRequests) {
        return new BatchResponseDTO<IncomingDTO>().setResults(
            mapper.toResultsDtos(incomingService.batch(mapper.toOperations(incomingRequests.getOperations())))
        );
    }
}
