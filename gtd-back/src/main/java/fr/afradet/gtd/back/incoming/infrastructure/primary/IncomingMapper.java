package fr.afradet.gtd.back.incoming.infrastructure.primary;

import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchOperationDTO;
import fr.afradet.gtd.back.shared.batch.infrastructure.primary.BatchResultDTO;
import fr.afradet.gtd.back.shared.mapping.GlobalMapperConfig;
import fr.afradet.gtd.back.incoming.domain.Incoming;
import fr.afradet.gtd.back.shared.batch.domain.BatchOperation;
import fr.afradet.gtd.back.shared.batch.domain.BatchResult;
import fr.afradet.gtd.back.shared.batch.domain.OperationType;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(config = GlobalMapperConfig.class)
public interface IncomingMapper {

    Incoming toPojo(IncomingDTO dto);

    IncomingDTO toDto(Incoming pojo);

    List<Incoming> toPojos(List<IncomingDTO> dtos);

    List<IncomingDTO> toDtos(List<Incoming> pojos);

    default BatchOperation<Incoming> toOperation(BatchOperationDTO<IncomingDTO> operation) {
        return new BatchOperation<Incoming>()
                .setOperation(OperationType.fromMethod(operation.getMethod()))
                .setBody(this.toPojo(operation.getBody()));
    }

    List<BatchOperation<Incoming>> toOperations(List<BatchOperationDTO<IncomingDTO>> operations);

    default BatchResultDTO<IncomingDTO> toResultDto(BatchResult<Incoming> result) {
        return new BatchResultDTO<IncomingDTO>()
                .setStatusCode(result.getHttpStatus().value())
                .setBody(this.toDto(result.getBody()));
    }

    List<BatchResultDTO<IncomingDTO>> toResultsDtos(List<BatchResult<Incoming>> results);
}
