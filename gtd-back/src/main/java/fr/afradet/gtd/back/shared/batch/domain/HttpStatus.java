package fr.afradet.gtd.back.shared.batch.domain;

public enum HttpStatus {
  OK(200),
  NOT_FOUND(404);

  private final int value;

  public int value() {
    return this.value;
  }

  HttpStatus(int value) {
    this.value = value;
  }
}
