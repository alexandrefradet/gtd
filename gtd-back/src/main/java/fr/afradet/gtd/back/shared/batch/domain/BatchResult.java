package fr.afradet.gtd.back.shared.batch.domain;

public class BatchResult<T> {

    private Integer order;
    private T body;
    private HttpStatus httpStatus;

    public Integer getOrder() {
        return order;
    }

    public BatchResult<T> setOrder(Integer order) {
        this.order = order;
        return this;
    }

    public T getBody() {
        return body;
    }

    public BatchResult<T> setBody(T body) {
        this.body = body;
        return this;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public BatchResult<T> setHttpStatus(HttpStatus status) {
        this.httpStatus = status;
        return this;
    }
}
