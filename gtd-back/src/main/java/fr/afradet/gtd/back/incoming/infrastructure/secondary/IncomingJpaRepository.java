package fr.afradet.gtd.back.incoming.infrastructure.secondary;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IncomingJpaRepository extends JpaRepository<IncomingEntity, Long> {

  List<IncomingEntity> findAllByCompleted(boolean completed);
}
