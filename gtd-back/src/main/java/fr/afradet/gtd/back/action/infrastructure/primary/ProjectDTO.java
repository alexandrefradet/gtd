package fr.afradet.gtd.back.action.infrastructure.primary;

import jakarta.validation.constraints.NotBlank;
import java.util.List;
import lombok.Data;

@Data
public class ProjectDTO {

    private Long id;
    @NotBlank
    private String name;
    private String status;
    private List<NextActionDTO> actions;
}
