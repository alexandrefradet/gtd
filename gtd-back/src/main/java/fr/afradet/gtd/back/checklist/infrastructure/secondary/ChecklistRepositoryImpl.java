package fr.afradet.gtd.back.checklist.infrastructure.secondary;

import fr.afradet.gtd.back.checklist.domain.Checklist;
import fr.afradet.gtd.back.checklist.domain.ChecklistLight;
import fr.afradet.gtd.back.checklist.domain.ChecklistRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ChecklistRepositoryImpl implements ChecklistRepository {

  private final ChecklistJpaRepository repository;

  public ChecklistRepositoryImpl(ChecklistJpaRepository repository) {
    this.repository = repository;
  }

  @Override
  public List<ChecklistLight> findAll() {
    return repository.findByOrderByCreationDate().stream()
        .map(l -> new ChecklistLight().setId(l.id()).setName(l.name())).toList();
  }

  @Override
  public Optional<Checklist> findById(Long id) {
    return repository.findById(id).map(ChecklistEntity::toDomain);
  }

  @Override
  public Checklist save(Checklist checklist) {
    return repository.save(ChecklistEntity.fromDomain(checklist)).toDomain();
  }

  @Override
  public void deleteById(Long id) {
    repository.deleteById(id);
  }
}
