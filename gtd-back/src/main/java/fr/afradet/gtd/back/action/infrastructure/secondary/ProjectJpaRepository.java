package fr.afradet.gtd.back.action.infrastructure.secondary;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectJpaRepository extends JpaRepository<ProjectEntity, Long> {
  List<ProjectEntity> findAllByCompleted(boolean b);

}
