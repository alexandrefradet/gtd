package fr.afradet.gtd.back.shared.exception.infrastructure.primary;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ApiError {

    private String errorMessage;
    private Map<String, String> errorDetails = new HashMap<>();
}
