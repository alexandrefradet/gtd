package fr.afradet.gtd.back.shared.batch.domain;

public class BatchOperation<T> {

    private Integer order;
    private T body;
    private OperationType operation;

    public Integer getOrder() {
        return order;
    }

    public BatchOperation<T> setOrder(Integer order) {
        this.order = order;
        return this;
    }

    public T getBody() {
        return body;
    }

    public BatchOperation<T> setBody(T body) {
        this.body = body;
        return this;
    }

    public OperationType getOperation() {
        return operation;
    }

    public BatchOperation<T> setOperation(OperationType operation) {
        this.operation = operation;
        return this;
    }
}
