package fr.afradet.gtd.back.shared.batch.infrastructure.primary;

import java.util.List;

public class BatchRequestDTO<T> {

    private List<BatchOperationDTO<T>> operations;

    public List<BatchOperationDTO<T>> getOperations() {
        return operations;
    }

    public BatchRequestDTO<T> setOperations(List<BatchOperationDTO<T>> operations) {
        this.operations = operations;
        return this;
    }
}
