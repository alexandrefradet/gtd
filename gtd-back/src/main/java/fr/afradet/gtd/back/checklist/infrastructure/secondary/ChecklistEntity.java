package fr.afradet.gtd.back.checklist.infrastructure.secondary;

import fr.afradet.gtd.back.checklist.domain.Checklist;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.Data;

@Data
@Entity
@Table(name = "t_checklist")
public class ChecklistEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private LocalDate creationDate;
  private LocalDate lastResetDate;
  @Enumerated(EnumType.STRING)
  private ChronoUnit resetFrequencyUnit;
  private Integer resetFrequencyValue;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "checklist_id")
  @OrderBy("id")
  private List<ChecklistElementEntity> checklistElements = new ArrayList<>();

  public static ChecklistEntity fromDomain(Checklist checklist) {
    return new ChecklistEntity()
        .setId(checklist.getId())
        .setName(checklist.getName())
        .setCreationDate(checklist.getCreationDate())
        .setLastResetDate(checklist.getLastResetDate())
        .setResetFrequencyUnit(checklist.getResetFrequencyUnit())
        .setResetFrequencyValue(checklist.getResetFrequencyValue())
        .setChecklistElements(
            checklist.getChecklistElements() != null ?
                checklist.getChecklistElements().stream().map(ChecklistElementEntity::fromDomain)
                    .toList() :
                Collections.emptyList()
        );
  }

  public Checklist toDomain() {
    return new Checklist()
        .setId(id)
        .setName(name)
        .setCreationDate(creationDate)
        .setLastResetDate(lastResetDate)
        .setResetFrequencyUnit(resetFrequencyUnit)
        .setResetFrequencyValue(resetFrequencyValue)
        .setChecklistElements(
            checklistElements != null ?
                checklistElements.stream().map(ChecklistElementEntity::toDomain).toList() :
                Collections.emptyList()
        );
  }
}
