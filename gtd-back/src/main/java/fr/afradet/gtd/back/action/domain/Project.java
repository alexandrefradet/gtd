package fr.afradet.gtd.back.action.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class Project {

    private Long id;
    private String name;
    private LocalDateTime creationDate;
    private LocalDateTime completionDate;
    private EProjectStatus status;
    private boolean completed;
    private List<NextAction> actions = new ArrayList<>();

    public Project complete(LocalDateTime timestamp) {
        completed = true;
        completionDate = timestamp;
        status = EProjectStatus.COMPLETED;
        return this;
    }
}
