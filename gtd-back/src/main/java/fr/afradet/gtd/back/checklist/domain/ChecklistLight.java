package fr.afradet.gtd.back.checklist.domain;

import lombok.Data;

@Data
public class ChecklistLight {

  private Long id;
  private String name;
}
