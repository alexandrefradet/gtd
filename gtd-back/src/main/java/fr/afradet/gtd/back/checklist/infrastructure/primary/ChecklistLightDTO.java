package fr.afradet.gtd.back.checklist.infrastructure.primary;

import jakarta.validation.constraints.NotBlank;

public record ChecklistLightDTO(Long id, @NotBlank String name){}
