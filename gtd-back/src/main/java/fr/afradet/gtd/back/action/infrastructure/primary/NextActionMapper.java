package fr.afradet.gtd.back.action.infrastructure.primary;

import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.Project;
import fr.afradet.gtd.back.shared.mapping.GlobalMapperConfig;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(config = GlobalMapperConfig.class)
public interface NextActionMapper {

    NextAction toPojo(NextActionDTO dto);

    NextActionDTO toDto(NextAction pojo);

    List<NextAction> toPojos(List<NextActionDTO> dtos);

    List<NextActionDTO> toDtos(List<NextAction> pojos);

    Project map(ProjectLightDTO project);

    ProjectLightDTO map(Project project);
}
