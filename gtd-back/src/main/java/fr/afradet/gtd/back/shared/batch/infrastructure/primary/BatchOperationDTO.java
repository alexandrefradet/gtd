package fr.afradet.gtd.back.shared.batch.infrastructure.primary;


import fr.afradet.gtd.back.shared.batch.domain.HttpMethod;

public class BatchOperationDTO<T> {

    private T body;
    private HttpMethod method;

    public T getBody() {
        return body;
    }

    public BatchOperationDTO<T> setBody(T body) {
        this.body = body;
        return this;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public BatchOperationDTO<T> setMethod(HttpMethod method) {
        this.method = method;
        return this;
    }
}
