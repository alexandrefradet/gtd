package fr.afradet.gtd.back.shared.batch.domain;

import java.util.Arrays;

public enum OperationType {
    CREATE(HttpMethod.POST),
    UPDATE(HttpMethod.PUT),
    DELETE(HttpMethod.DELETE);

    private final HttpMethod method;

    public HttpMethod method() {
        return method;
    }

    OperationType(HttpMethod method) {
        this.method = method;
    }

    public static OperationType fromMethod(HttpMethod method) {
        return Arrays.stream(OperationType.values())
                .filter(o -> o.method().equals(method))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Method %s doesn't match any operation", method.name())));
    }
}
