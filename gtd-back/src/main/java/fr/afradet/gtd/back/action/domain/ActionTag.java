package fr.afradet.gtd.back.action.domain;

import lombok.Data;

@Data
public class ActionTag {

    private Long id;
    private String label;
    private String color;
    private String icon;
}
