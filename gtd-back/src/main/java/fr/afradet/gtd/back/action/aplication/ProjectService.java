package fr.afradet.gtd.back.action.aplication;

import fr.afradet.gtd.back.action.domain.EProjectStatus;
import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.NextActionRepository;
import fr.afradet.gtd.back.action.domain.Project;
import fr.afradet.gtd.back.action.domain.ProjectRepository;
import fr.afradet.gtd.back.shared.collection.Pair;
import fr.afradet.gtd.back.shared.exception.domain.ResourceNotFoundException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final NextActionRepository nextActionRepository;

    public ProjectService(ProjectRepository projectRepository, NextActionRepository nextActionRepository) {
        this.projectRepository = projectRepository;
        this.nextActionRepository = nextActionRepository;
    }

    public List<Project> getAllProjects() {
        return projectRepository.findAllByCompleted(false);
    }

    @Transactional
    public void delete(Long id) {
        if (!projectRepository.existsById(id)) {
            throw new ResourceNotFoundException();
        }
        deleteForProject(id);
        projectRepository.findById(id)
            .ifPresent(p -> projectRepository.save(p.complete(LocalDateTime.now())));
    }

    @Transactional
    public Pair<Project, List<NextAction>> create(Project newProject, List<NextAction> nextActions) {
        Project savedProject;

        if (nextActions != null && !nextActions.isEmpty()) {
            // TODO improve
            var newAction = nextActions.get(0);
            newAction.setActionPosition(0);
            newProject.setStatus(newAction.isCompleted() ? EProjectStatus.TASK_NEEDED : EProjectStatus.TASK_TODO);
            savedProject = projectRepository.save(newProject);
            newAction.setProject(savedProject);
            return Pair.of(savedProject, List.of(nextActionRepository.save(newAction)));
        } else {
            newProject.setStatus(EProjectStatus.TASK_NEEDED);
            savedProject = projectRepository.save(newProject);
            return Pair.of(savedProject, Collections.emptyList());
        }
    }

    public void deleteForProject(Long projectId) {
        var now = LocalDateTime.now();
        nextActionRepository.saveAll(
            nextActionRepository.findByProjectId(projectId)
                .stream()
                .map(action -> action.complete(now))
                .toList()
        );
    }
}
