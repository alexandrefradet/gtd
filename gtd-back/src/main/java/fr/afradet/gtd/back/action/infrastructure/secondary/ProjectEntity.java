package fr.afradet.gtd.back.action.infrastructure.secondary;

import fr.afradet.gtd.back.action.domain.EProjectStatus;
import fr.afradet.gtd.back.action.domain.Project;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "t_project")
public class ProjectEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private LocalDateTime creationDate;
  private LocalDateTime completionDate;
  @Enumerated(EnumType.STRING)
  private EProjectStatus status;
  private boolean completed;
  @OneToMany(mappedBy = "project", fetch = FetchType.EAGER)
  @OrderBy("actionPosition")
  private List<NextActionEntity> actions = new ArrayList<>();

  public static ProjectEntity fromDomain(Project project) {
    return new ProjectEntity()
        .setId(project.getId())
        .setName(project.getName())
        .setCreationDate(project.getCreationDate())
        .setCompletionDate(project.getCompletionDate())
        .setStatus(project.getStatus())
        .setCompleted(project.isCompleted());
  }

  public Project toDomain() {
    return new Project()
        .setId(id)
        .setName(name)
        .setCreationDate(creationDate)
        .setCompletionDate(completionDate)
        .setStatus(status)
        .setCompleted(completed);
  }

  public Project toDomainWithAction() {
    return this.toDomain()
        .setActions(
            Optional.ofNullable(actions)
                .map(a -> a.stream().map(NextActionEntity::toDomain).collect(Collectors.toList()))
                .orElse(Collections.emptyList())
        );
  }

  public ProjectEntity addAction(NextActionEntity action) {
    actions.add(action);
    action.setProject(this);
    return this;
  }

  public ProjectEntity removeAction(NextActionEntity action) {
    actions.remove(action);
    action.setProject(null);
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProjectEntity project = (ProjectEntity) o;
    return completed == project.completed && Objects.equals(id, project.id)
        && Objects.equals(name, project.name) && Objects.equals(creationDate,
        project.creationDate) && Objects.equals(completionDate, project.completionDate)
        && status == project.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, creationDate, completionDate, status, completed);
  }

  @Override
  public String toString() {
    return "Project{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", creationDate=" + creationDate +
        ", completionDate=" + completionDate +
        ", status=" + status +
        ", completed=" + completed +
        '}';
  }
}
