package fr.afradet.gtd.back.action.infrastructure.primary;

import fr.afradet.gtd.back.action.domain.NextAction;
import fr.afradet.gtd.back.action.domain.Project;
import fr.afradet.gtd.back.shared.mapping.GlobalMapperConfig;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = GlobalMapperConfig.class, uses = NextActionMapper.class)
public interface ProjectMapper {

    Project toPojo(ProjectDTO dto);

    @Mapping(target = "actions", source = "actions")
    ProjectDTO toDto(Project pojo, List<NextAction> actions);

    List<Project> toPojos(List<ProjectDTO> dtos);

    List<ProjectDTO> toDtos(List<Project> pojo);
}
