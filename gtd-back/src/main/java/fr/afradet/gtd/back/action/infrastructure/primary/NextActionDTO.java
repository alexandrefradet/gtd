package fr.afradet.gtd.back.action.infrastructure.primary;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.Data;

@Data
public class NextActionDTO {

    private Long id;

    @NotBlank
    private String label;

    private String content;

    @NotNull
    private Boolean completed;

    private LocalDateTime creationDate;

    private LocalDateTime deadlineDate;

    private int actionPosition;

    private ProjectLightDTO project;

    @NotNull
    private String actionType;

    private Set<ActionTagDTO> actionTags;
}
