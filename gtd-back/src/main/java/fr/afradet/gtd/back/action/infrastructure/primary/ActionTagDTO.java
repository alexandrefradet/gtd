package fr.afradet.gtd.back.action.infrastructure.primary;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ActionTagDTO {

    private Long id;

    @NotBlank
    private String label;

    @NotBlank
    private String color;

    @NotBlank
    private String icon;
}
