package fr.afradet.gtd.back.action.domain;

import java.util.List;
import java.util.Optional;

public interface ActionTagRepository {

    List<ActionTag> findAll();

    ActionTag save(ActionTag actionTag);

    Optional<ActionTag> findById(Long id);

}
