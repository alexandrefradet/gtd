package fr.afradet.gtd.back.incoming.domain;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class Incoming {

  private Long id;
  private String title;
  private String content;
  private LocalDateTime creationDate;
  private LocalDateTime completionDate;
  private Boolean completed;

  public Incoming complete() {
    this.completed = true;
    this.completionDate = LocalDateTime.now();
    return this;
  }
}

