package fr.afradet.gtd.back.checklist.infrastructure.primary;

import fr.afradet.gtd.back.shared.mapping.GlobalMapperConfig;
import fr.afradet.gtd.back.checklist.domain.ChecklistElement;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(config = GlobalMapperConfig.class)
public interface ChecklistElementMapper {

    ChecklistElement toPojo(ChecklistElementDTO dto);

    ChecklistElementDTO toDto(ChecklistElement pojo);

    List<ChecklistElement> toPojos(List<ChecklistElementDTO> dtos);

    List<ChecklistElementDTO> toDtos(List<ChecklistElement> pojos);
}
