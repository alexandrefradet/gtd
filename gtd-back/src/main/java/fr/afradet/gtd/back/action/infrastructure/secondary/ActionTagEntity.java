package fr.afradet.gtd.back.action.infrastructure.secondary;

import fr.afradet.gtd.back.action.domain.ActionTag;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "t_action_tag")
public class ActionTagEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String label;
  private String color;
  private String icon;

  public static ActionTagEntity fromDomain(ActionTag tag) {
    return new ActionTagEntity()
        .setId(tag.getId())
        .setLabel(tag.getLabel())
        .setColor(tag.getColor())
        .setIcon(tag.getIcon());
  }

  public ActionTag toDomain() {
    return new ActionTag()
        .setId(id)
        .setLabel(label)
        .setColor(color)
        .setIcon(icon);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ActionTagEntity actionTag = (ActionTagEntity) o;
    return Objects.equals(id, actionTag.id) && Objects.equals(label,
        actionTag.label) && Objects.equals(color, actionTag.color)
        && Objects.equals(icon, actionTag.icon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, label, color, icon);
  }
}
