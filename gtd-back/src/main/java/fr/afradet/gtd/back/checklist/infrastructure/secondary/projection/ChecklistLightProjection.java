package fr.afradet.gtd.back.checklist.infrastructure.secondary.projection;

public record ChecklistLightProjection(Long id, String name) {

}
