package fr.afradet.gtd.back.checklist.domain;

import java.util.List;
import java.util.Optional;

public interface ChecklistRepository {

  List<ChecklistLight> findAll();

  Optional<Checklist> findById(Long id);

  Checklist save(Checklist ck);

  void deleteById(Long id);
}
