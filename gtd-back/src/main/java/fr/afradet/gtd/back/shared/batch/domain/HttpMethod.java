package fr.afradet.gtd.back.shared.batch.domain;

public enum HttpMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
}
