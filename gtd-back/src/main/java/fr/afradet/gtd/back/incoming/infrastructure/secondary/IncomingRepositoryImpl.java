package fr.afradet.gtd.back.incoming.infrastructure.secondary;

import fr.afradet.gtd.back.incoming.domain.Incoming;
import fr.afradet.gtd.back.incoming.domain.IncomingRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class IncomingRepositoryImpl implements IncomingRepository {

    private final IncomingJpaRepository repository;

    public IncomingRepositoryImpl(IncomingJpaRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Incoming> findAllByCompleted(boolean completed) {
        return repository.findAllByCompleted(completed)
            .stream()
            .map(IncomingEntity::toDomain)
            .toList();
    }

    @Override
    public Optional<Incoming> findById(Long id) {
        return repository.findById(id).map(IncomingEntity::toDomain);
    }

    @Override
    public Incoming save(Incoming incoming) {
        return repository.save(IncomingEntity.fromDomain(incoming)).toDomain();
    }


    @Override
    public List<Incoming> saveAll(List<Incoming> incomings) {
        return repository.saveAll(incomings.stream().map(IncomingEntity::fromDomain).toList())
            .stream()
            .map(IncomingEntity::toDomain)
            .toList();
    }

}
