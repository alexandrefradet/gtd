package fr.afradet.gtd.back.checklist.infrastructure.primary;

import fr.afradet.gtd.back.checklist.domain.ChecklistActionType;
import fr.afradet.gtd.back.checklist.application.ChecklistService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("rest/checklists")
@PreAuthorize("hasRole('GTD_USER') or hasRole('GTD_ADMIN')")
public class ChecklistController {

    private final ChecklistService checklistService;
    private final ChecklistMapper mapper;

    public ChecklistController(ChecklistService checklistService, ChecklistMapper mapper) {
        this.checklistService = checklistService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<ChecklistLightDTO> getAllChecklists() {
        return mapper.toLightDtos(checklistService.getAllChecklist());
    }

    @GetMapping("/{id}")
    public ChecklistDTO getChecklist(@PathVariable("id") Long id) {
        return mapper.toDto(checklistService.getChecklist(id));
    }

    @PostMapping
    public ChecklistDTO createChecklist(@RequestBody @Valid ChecklistDTO dto) {
        return mapper.toDto(checklistService.createChecklist(mapper.toPojo(dto)));
    }

    @PutMapping("/{id}")
    public ChecklistDTO updateChecklist(@PathVariable("id") Long id, @RequestBody @Valid ChecklistDTO dto) {
        if (!id.equals(dto.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return mapper.toDto(checklistService.updateChecklist(mapper.toPojo(dto)));
    }

    @PostMapping("/{id}")
    public ChecklistDTO actionCheckList(@PathVariable("id") Long id, @RequestParam ChecklistActionType action) {
        return mapper.toDto(checklistService.execute(id, action));
    }

    @DeleteMapping("/{id}")
    public void deleteChecklist(@PathVariable("id") Long id) {
        checklistService.deleteChecklist(id);
    }
}
