package fr.afradet.gtd.back.checklist.infrastructure.secondary;

import fr.afradet.gtd.back.checklist.domain.ChecklistElement;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "t_checklist_element")
public class ChecklistElementEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String label;
  private boolean checked;

  public ChecklistElement toDomain() {
    return new ChecklistElement()
        .setId(id)
        .setLabel(label)
        .setChecked(checked);
  }

  public static ChecklistElementEntity fromDomain(ChecklistElement checklistElement) {
    return new ChecklistElementEntity()
        .setId(checklistElement.getId())
        .setLabel(checklistElement.getLabel())
        .setChecked(checklistElement.isChecked());
  }
}
