package fr.afradet.gtd.back.checklist.infrastructure.secondary;

import fr.afradet.gtd.back.checklist.infrastructure.secondary.projection.ChecklistLightProjection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChecklistJpaRepository extends JpaRepository<ChecklistEntity, Long> {

  List<ChecklistLightProjection> findByOrderByCreationDate();

  @EntityGraph(attributePaths = { "checklistElements" })
  Optional<ChecklistEntity> findById(Long id);

  // TODO : Only use for test assertions, cleanup, move to test classes
  @EntityGraph(attributePaths = { "checklistElements" })
  List<ChecklistEntity> findAll();
}
