package fr.afradet.gtd.back.action.infrastructure.primary;

import fr.afradet.gtd.back.action.domain.ActionTag;
import fr.afradet.gtd.back.shared.mapping.GlobalMapperConfig;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(config = GlobalMapperConfig.class)
public interface ActionTagMapper {

    ActionTag toPojo(ActionTagDTO dto);

    ActionTagDTO toDto(ActionTag pojo);

    List<ActionTag> toPojos(List<ActionTagDTO> dtos);

    List<ActionTagDTO> toDtos(List<ActionTag> pojos);
}
