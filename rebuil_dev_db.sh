#!/bin/bash

docker run -d --rm --name application-postgres-dev -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=application -p 5432:5432 postgres
